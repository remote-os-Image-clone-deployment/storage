# Storage

Storage stores the OS images, it hosts two servers, which are as follows:
1. NFS server : This servers the images from  nfs shared directory "/nfsroot/images".
2. HTTP server: This server communicates with the web app server and controls the multicast imaging.

This repository/sub-project contains http server developed using django as backend and multicasting tool [UDPcast](udpcast.linux.lu/) for multicast image deployment . The REST APIs over http is used t communicate between rosim web app and the storage to synchronise multicast session with the clients.
Two API endpoints were created at the storage server for following communication:
1.  ”/multicast/createSession”:  When Web Server receives multicast task from admin,it  communicates  the  task  to  the  storage  server  with  image  name,  number  of  re-ceivers using this API and gets the allocated multicast address and port numberas  reply  from  storage  server.  The  storage  server  allocates  multicast  IP  address 239.255.0.0/16.
2.  ”/multicast/startSession”:  When a client belonging to multicast group boots andasks  for  task,  the  web  server  gives  the  multicast  group  details  to  client.   It  then informs storage server to start multicast session so the clients can join using this API.

[UDPcast](udpcast.linux.lu/) is a open-source multicast file transfer tool that can send data simultaneously to many clients in a LAN.UDPcast is used to implement multicast. 
UDPcast tool contains mainly two programs :
1. Udp-sender: Udp-sender is used to  multicast a disk image to multiple udp-receivers on the local LAN. In order to do this, it uses Ethernet multicast, so that all receivers profit from the same physical datastream. Udp-sender is used at the storage server. It is included in multicast.sh file.
2. Udp-reciever: Udp-receiver is used to receive files sent by udp-sender i.e. the disk image. Udp-reciever tool is integrated with IDOS.


     
     