from django.contrib import admin
from django.urls import path, include
from . import views


from django.conf.urls.static import  static
from django.conf import settings

urlpatterns = [
 
    path('createSession', views.createSession),
    path('startSession',views.startSession),
    # path('imagedeployment',views.ImageDeployment),
]