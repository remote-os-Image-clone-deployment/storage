from django.db import models

# Create your models here.
class Multicast(models.Model):
    taskId=models.IntegerField(primary_key=True)
    mcastAddr=models.CharField(unique=True,max_length=20, default=None)
    port=models.IntegerField(unique=True)
    imageName= models.CharField(max_length=20, null=False, default=None)
    receivers=models.IntegerField()
    started=models.BooleanField(default=False)

class StorageSettings(models.Model):
    web_server_ip=models.CharField(max_length=20, default="192.168.56.100")
    web_server_port=models.IntegerField(default=8080)
    udpcast_interface=models.CharField(max_length=20, default="vboxnet0")
