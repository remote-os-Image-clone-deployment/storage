from django.shortcuts import render
from django.http import JsonResponse
from multicast.models import Multicast, StorageSettings
from django.views.decorators.csrf import ensure_csrf_cookie
import os
import threading
import random 
from django.db.models import Q
import requests
# web_server="192.168.56.102:8080"
# Create your views here.
#threads
#synchronisation in updating and creating objects db
#limiting no.of multicast sessions
# full interaction of centralised server, storage and client for multicasting
def createSession(request):
    imageName = request.POST.get('imageName')
    taskId = request.POST.get('taskId')
    receivers = request.POST.get('receivers')
    settingsquery=StorageSettings.objects.filter(id=1) 
    if not settingsquery.exists(): 
        StorageSettings.objects.create() 				 #creates settings object with default values when first opened and then the user accesses the page
    session=Multicast.objects.filter(taskId=taskId)
    di={}

    if not session.exists():
        ip,port=create_ip_port()
        sessionInstance=Multicast.objects.create(taskId=taskId, mcastAddr=ip, port=port,imageName=imageName, receivers=receivers)
        # t1=threading.Thread(target=execute, args=(sessionInstance,))
        # t1.start()
        di['port']=port
        di['ip']=ip
    else:
        print(session[0])
        di['port']=session[0].port
        di['ip']=session[0].mcastAddr
        print("Session is already created")
    return JsonResponse(di)
    
def startSession(request):
    taskId = request.POST.get('taskId')
    session=Multicast.objects.filter(taskId=taskId)
    di={}

    if  session.exists() and not session[0].started:
        sessionInstance=session[0]
        sessionInstance.started=True
        sessionInstance.save()
        print(session[0].started)
        t1=threading.Thread(target=execute, args=(sessionInstance,))
        t1.start()
        di['status']="started"
    elif session.exists() and session[0].started: #prevent multiple starts and for status of started session
        di['status']="already started please join"
    else:
        di['status']="not found"
    return JsonResponse(di)


def execute(sessionInstance):
    settingsInstance=StorageSettings.objects.get(id=1)
    exec_string = f' bash multicast/multicast.sh {sessionInstance.mcastAddr} {sessionInstance.port} {sessionInstance.imageName} {sessionInstance.receivers} {settingsInstance.udpcast_interface}'
    # exec_string=f'pwd'
    print('Executing',exec_string)
    os.system(exec_string)   
    print("Session over",exec_string)
    web_server=settingsInstance.web_server_ip + ":" + str(settingsInstance.web_server_port)
    url="http://"+web_server+"/tasks/taskstatus"
    data={}
    data['taskId']=sessionInstance.taskId
    data['status']=0
    r=requests.post(url,data=data)
    if r.status_code==200:
        print(r.text)
    sessionInstance.delete()

def create_ip_port():
    ip="239.255.0." #according to RFC5771, RFC 2365, IPv4 local scope
    rint=random.randint(1,255)
    ip=ip+str(rint)
    port=random.randint(9000,10000)
    if port%2!=0: # the port number for udpcast must be evens
        port=port+1
    temp=Multicast.objects.filter(Q(mcastAddr=ip)|Q(port=port))
    while temp.exists():
        if temp[0].mcastAddr==ip:
            rint=random.randint(1,255)
            ip="232.0.0."+str(rint)
        if temp[0].port==port:
            port=random.randint(9000,10000)
            if port%2!=0:
                port=port+1
    return ip, port