from django.apps import AppConfig


class MulticastConfig(AppConfig):
    name = 'multicast'
