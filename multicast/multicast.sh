#!/bin/bash

mcast_data_address="$1"
port_base="$2"
IMAGE_CHOICE="$3"
receivers="$4"
interface="$5"
images_dir="/nfsroot/images"
 for i in `ls $images_dir/$IMAGE_CHOICE/*.img` ; do
    echo "serving $i" && sleep 3
    udp-sender --file $i  --interface $interface --full-duplex --mcast-data-address $mcast_data_address --portbase $port_base --min-receivers $receivers --no-progress 
done
